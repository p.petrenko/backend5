<?php

header('Content-Type: text/html; charset=UTF-8');

//Only report fatal errors and parse errors.
error_reporting(E_ERROR | E_PARSE);

session_start();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  //include('form.php');
  $response_message = '';
  $errors_fields = '';

  if(!empty($_SESSION['login'])) {
    $user = 'u20323';
    $pass = '6300522';
    $db = new PDO('mysql:host=localhost;dbname=u20323', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $stmt1 = $db->prepare("SELECT name, email, birthday, gender, limb_number, biography FROM Form WHERE form_id = ?"); 
    $stmt1->execute([$_SESSION['form_id']]);
    $db_response = $stmt1->fetch(PDO::FETCH_ASSOC);
        
    $name_field_value =  strip_tags($db_response['name']);
    $email_field_value =  strip_tags($db_response['email']);
    $bd_field_value =  strip_tags($db_response['birthday']);
    $gender_field_value =  strip_tags($db_response['gender']);
    $limb_number_field_value =  strip_tags($db_response['limb_number']);
    $biography_field_value =  strip_tags($db_response['biography']);

    $sp1_field_value = FALSE;
    $sp2_field_value = FALSE;
    $sp3_field_value = FALSE;
    $stmt2 = $db->prepare("SELECT ability_id FROM Form_Abilities WHERE form_id = ?"); 
    $stmt2->execute([$_SESSION['form_id']]);
    while($db_response = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      switch($db_response['ability_id']) {
        case 1:
          $sp1_field_value = TRUE;
          break;
        case 2: 
          $sp2_field_value = TRUE;
          break;
        case 3: 
          $sp3_field_value = TRUE;
          break;
      }
    } 
  }
  else {
    $name_field_value = $_COOKIE['last_success_name'];
    $email_field_value =  $_COOKIE['last_success_email'];
    $bd_field_value =  $_COOKIE['last_success_bd'];
    $gender_field_value =  $_COOKIE['last_success_gender'];
    $limb_number_field_value =  $_COOKIE['last_success_limb_number'];
    $sp1_field_value =  $_COOKIE['last_succes_sp1'];
    $sp2_field_value =  $_COOKIE['last_succes_sp2'];
    $sp3_field_value =  $_COOKIE['last_succes_sp3'];
    $biography_field_value =  $_COOKIE['last_success_biography'];
  }
  
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    $response_message .= 'Спасибо, результаты сохранены.';

    if(empty($_SESSION['login'])) {
      //$_SESSION['login'] = $_COOKIE['client_login'];
      //Записываем ID пользователя.
      //session_id(substr($_SESSION['login'], 6, 16));

      $response_message .= '<br>Ваши данные для авторизации:
      <br>Логин: ' . $_COOKIE['client_login'] . 
      '<br>Пароль: ' . $_COOKIE['client_password'];
    }
     setcookie('client_login', '', 100000);
     setcookie('client_password', '', 100000); 
  }
  else {
    $errors = FALSE;
    $response_message .= "Отправка данных прервана из-за следующих ошибок: <br/>";
    $gender_field_value =  $_COOKIE['gender_last_value'];
    $limb_number_field_value =  $_COOKIE['limb_number_last_value'];
    $sp1_field_value = $_COOKIE['sp1_last_value'];
    $sp2_field_value = $_COOKIE['sp2_last_value'];
    $sp3_field_value = $_COOKIE['sp3_last_value'];
    $biography_field_value =  $_COOKIE['biography_last_value'];
    
    $name_field_value = $_COOKIE['name_last_value'];;
    if(!empty($_COOKIE['name_error'])) {
      $response_message .= 'Вы не заполнили имя.<br/>';
      $errors_fields .= '#label_for_name';
      $errors = TRUE;
    }

    $email_field_value = $_COOKIE['email_last_value'];
    if(!empty($_COOKIE['email_error'])) {
      $response_message .= 'Вы неверно заполнили email или не ввели данные.<br/>';
      if(!empty($errors_fields))
        $errors_fields .= ', ';
      $errors_fields .= '#label_for_email';
      $errors = TRUE;
    }

    $bd_field_value = $_COOKIE['birthday_last_value'];
    if(!empty($_COOKIE['birthday_error'])) {
      $response_message .= 'Вы неверно заполнили дату рождения или не ввели данные.<br/>';
      if(!empty($errors_fields))
        $errors_fields .= ', ';
      $errors_fields .= '#label_for_birthday';
      $errors = TRUE;
    }
    if(!empty($_COOKIE['gender_error'])) {
      $response_message .= 'Вы неверно заполнили пол.<br/>';
      $errors = TRUE;
    }
    if(!empty($_COOKIE['limb_number_error'])) {
      $response_message .= 'Вы ввели недопустимое количество конечностей.<br/>';
      $errors = TRUE;
    }
    if(!empty($_COOKIE['superpowers_error'])) {
      $response_message .= 'Вы ввели неверные суперспособности.<br/>';
      $errors = TRUE;
    }
    if(!empty($_COOKIE['contract_error'])) {
        $response_message .= 'Вы не ознакомились с контрактом.<br/>';
        if(!empty($errors_fields))
          $errors_fields .= ', ';
        $errors_fields .= '#label_for_cb';
        $errors = TRUE;
    }
    if(!$errors) {
      $response_message = '';
      if(!empty($_SESSION['login'])) {
        $user = 'u20323';
        $pass = '6300522';
        $db = new PDO('mysql:host=localhost;dbname=u20323', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        $stmt1 = $db->prepare("SELECT name, email, birthday, gender, limb_number, biography FROM Form WHERE form_id = ?"); 
        $stmt1->execute([$_SESSION['form_id']]);
        $db_response = $stmt1->fetch(PDO::FETCH_ASSOC);
            
        $name_field_value =  strip_tags($db_response['name']);
        $email_field_value =  strip_tags($db_response['email']);
        $bd_field_value =  strip_tags($db_response['birthday']);
        $gender_field_value =  strip_tags($db_response['gender']);
        $limb_number_field_value =  strip_tags($db_response['limb_number']);
        $biography_field_value =  strip_tags($db_response['biography']);

        $sp1_field_value = FALSE;
        $sp2_field_value = FALSE;
        $sp3_field_value = FALSE;
        $stmt2 = $db->prepare("SELECT ability_id FROM Form_Abilities WHERE form_id = ?"); 
        $stmt2->execute([$_SESSION['form_id']]);
        while($db_response = $stmt2->fetch(PDO::FETCH_ASSOC)) {
          switch($db_response['ability_id']) {
            case 1:
              $sp1_field_value = TRUE;
              break;
            case 2: 
              $sp2_field_value = TRUE;
              break;
            case 3: 
              $sp3_field_value = TRUE;
              break;
          }
        } 
      }
      else {
        $name_field_value = $_COOKIE['last_success_name'];
        $email_field_value =  $_COOKIE['last_success_email'];
        $bd_field_value =  $_COOKIE['last_success_bd'];
        $gender_field_value =  $_COOKIE['last_success_gender'];
        $limb_number_field_value =  $_COOKIE['last_success_limb_number'];
        $sp1_field_value =  $_COOKIE['last_succes_sp1'];
        $sp2_field_value =  $_COOKIE['last_succes_sp2'];
        $sp3_field_value =  $_COOKIE['last_succes_sp3'];
        $biography_field_value =  $_COOKIE['last_success_biography'];
      }
    }
  }
  include('form.php');
  if(!empty($response_message)) {
    print "<link href='show_mb.css' rel='stylesheet'>
            <div id='modal_blackout'>
              <div id='success_form_response'>
                  {$response_message}
              </div>
            </div>";
    if(!empty($errors_fields)) {
        print "<style>
          {$errors_fields} {
            text-decoration: underline 1px wavy rgba(193,56,0,1);
            -webkit-text-decoration-line: underline;
            -webkit-text-decoration-style: wavy;
            -webkit-text-decoration-color: rgba(193,56,0,1);
            padding-bottom: 1px;
            text-underline-position: under;
          }
          </style>";      
    }
  }
    exit();
}

$trimmedPost = [];

$valid_errors = FALSE;

foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimmedPost[$key] = trim($value);
	else
		$trimmedPost[$key] = $value;

setcookie('name_last_value', $trimmedPost['name'] , 0);
if (empty($trimmedPost['name'])) {
  //print('Заполните имя.<br/>');
  setcookie('name_error', 1 , 0);
  $valid_errors = TRUE;
}
else setcookie('name_error', '', 1);

setcookie('email_last_value', $trimmedPost['email'], 0);
if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
  //print('Заполните email.<br/>');
  setcookie('email_error', 1, 0);
  $valid_errors = TRUE;
}
else setcookie('email_error', '', 1);

setcookie('birthday_last_value', $trimmedPost['birthday'], 0);
if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
  //print('Заполните дату рождения.<br/>');
  setcookie('birthday_error', 1, 0);
  $valid_errors = TRUE;
}
else setcookie('birthday_error', '', 1);

setcookie('gender_last_value', $trimmedPost['gender'], 0);
if (!preg_match('/^[MF]$|^(NB)$/', $trimmedPost['gender'])) {
  //print('Заполните пол.<br/>');
  setcookie('gender_error', '1', 0);
  $valid_errors = TRUE;
}
else setcookie('gender_error', '', 1);

setcookie('limb_number_last_value', $trimmedPost['limb_number'], 0);
if (!preg_match('/^[0-5]$/', $trimmedPost['limb_number'])) {
  //print('Заполните количество конечностей.<br/>');
  setcookie('limb_number_error', '1', 0);
  $valid_errors = TRUE;
}
else setcookie('limb_number_error', '', 1);

setcookie('biography_last_value', $trimmedPost['biography'], 0);

setcookie('superpowers_error', '', 1);

setcookie('sp1_last_value', false, 0);
setcookie('sp2_last_value', false, 0);
setcookie('sp3_last_value', false, 0);

foreach ($trimmedPost['superpowers'] as $v) {
  if (!preg_match('/[1-3]/', $v)) {
    //print('Неверные суперспособности.<br/>');
    setcookie('superpowers_error', '1', 0);
    $valid_errors = TRUE;
  }
  else {
    $current_sp = 'sp' . $v . '_last_value';
    setcookie($current_sp, true, 0);
  }
}

if (!isset($trimmedPost['contract'])) {
  //print('Вы не ознакомились с контрактом.<br/>');
  setcookie('contract_error', '1', 0);
  $valid_errors = TRUE;
}
else setcookie('contract_error', '', 1);

/*foreach ($trimmedPost as $key => $value)
  echo $key,"=>",$value, '<br />';

foreach ($trimmedPost['superpowers'] as $v)
  echo $v,'<br />';*/

if ($valid_errors) {
  setcookie('save', '', 100000);
  header('Location: index.php');
  exit();
}

$user = 'u20323';
$pass = '6300522';
$db = new PDO('mysql:host=localhost;dbname=u20323', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $db->beginTransaction();

  if(empty($_SESSION['login'])) {
    $client_login = substr(uniqid('client', true), 0, 16);
    $client_password = substr(hash('sha1', $client_login . $trimmedPost['birthday']), 0, 10);
    $client_password_hash = hash('md5', $client_password);
    setcookie('client_login', $client_login);
    setcookie('client_password', $client_password);

    $stmt1 = $db->prepare("INSERT INTO Form SET name = ?, email = ?, birthday = ?, 
      gender = ? , limb_number = ?, biography = ?, login = ?, password_hash = ?");
    $stmt1 -> execute([$trimmedPost['name'], $trimmedPost['email'], $trimmedPost['birthday'], 
      $trimmedPost['gender'], $trimmedPost['limb_number'], $trimmedPost['biography'], $client_login, $client_password_hash]);
    $stmt2 = $db->prepare("INSERT INTO Form_Abilities SET form_id = ?, ability_id = ?");
    $id = $db->lastInsertId();
    foreach ($trimmedPost['superpowers'] as $s)
      $stmt2 -> execute([$id, $s]);
    $db->commit();
  }
  else {
    $stmt1 = $db->prepare("UPDATE Form SET name = ?, email = ?, birthday = ?, 
    gender = ? , limb_number = ?, biography = ? WHERE login = ?");
    $stmt1 -> execute([$trimmedPost['name'], $trimmedPost['email'], $trimmedPost['birthday'], 
      $trimmedPost['gender'], $trimmedPost['limb_number'], $trimmedPost['biography'], $_SESSION['login']]);

    $id = $_SESSION['form_id'];
    $stmt2 = $db->prepare("DELETE FROM Form_Abilities WHERE form_id = ?");
    $stmt2 -> execute([$id]);

    $stmt3 = $db->prepare("INSERT INTO Form_Abilities SET form_id = ?, ability_id = ?");
    foreach ($trimmedPost['superpowers'] as $s)
      $stmt3 -> execute([$id, $s]);
    $db->commit();
  }
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  $db->rollBack();
  setcookie('client_login', '', 1);
  setcookie('client_password', '', 1);
  exit();
}

setcookie('save', '1');

setcookie('last_success_name', $trimmedPost['name'], time()+3600 * 24 * 365);
setcookie('last_success_email', $trimmedPost['email'], time()+3600 * 24 * 365);
setcookie('last_success_bd', $trimmedPost['birthday'], time()+3600 * 24 * 365);
setcookie('last_success_gender', $trimmedPost['gender'], time()+3600 * 24 * 365);
setcookie('last_success_limb_number', $trimmedPost['limb_number'], time()+3600 * 24 * 365);
setcookie('last_succes_sp1', $_COOKIE['sp1_last_value'], time()+3600 * 24 * 365);
setcookie('last_succes_sp2', $_COOKIE['sp2_last_value'], time()+3600 * 24 * 365);
setcookie('last_succes_sp3', $_COOKIE['sp3_last_value'], time()+3600 * 24 * 365);
setcookie('last_success_biography', $trimmedPost['biography'], time()+3600 * 24 * 365);


setcookie('name_last_value', '', 1);
setcookie('email_last_value', '', 1);
setcookie('birthday_last_value', '', 1);
setcookie('gender_last_value', '', 1);
setcookie('limb_number_last_value', '', 1);
setcookie('sp1_last_value', '', 1);
setcookie('sp2_last_value', '', 1);
setcookie('sp3_last_value', '', 1);
setcookie('biography_last_value', '', 1);

header('Location: index.php');
