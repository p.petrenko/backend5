<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>WEB-Project</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <style>
    #main {
      max-width: 767px;
      text-align: center;
      position: fixed;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }

    #footer_site {
      position: fixed;
      bottom: 0;
      right: 0;
      left: 0;
      width: auto;
    }

    #header_site {
      position: fixed;
      top: 0;
      right: 0;
      left: 0;
      width: auto;
    }

    input[name="login"], input[name="password"] {
      border-radius: 5px;
      line-height: 1.2;
      font-weight: 500;
      font-size: 14px;
      padding: 26px 24px;
      height: 54px;
      border: 2px solid rgba(0,0,0,0.5);
    }

    input[name="login"]:focus, input[name="password"]:focus {
      border-color: #354e71;
      outline: 0;
      transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }

    input[type="submit"] {
      padding: 10px;
    }

    #modal_blackout {
      -webkit-animation: cssAnimation 2s forwards;
	    animation: cssAnimation 2s forwards;
    }
    </style>
  </head>
  <body>
    <header id="header_site">
      <div id="logo_and_name">
        <img id="logo" src="https://www.flaticon.com/svg/static/icons/svg/203/203678.svg" alt="Logo" />
        <h1>IndexPro</h1>
      </div>
    </header>
    <div class="container py-5 my-3 px-4" id="main">
      <form action="" method="post" id="login_form">
        <input name="login" placeholder="Введите логин"/>
        <input name="password" placeholder="Введите пароль" />
        <input type="submit" value="Войти" />
      </form>
    </div>
    <a id="before_footer"></a>
    <footer id="footer_site">
      <b>@In code we trust</b>
    </footer>
  </body>
</html>
